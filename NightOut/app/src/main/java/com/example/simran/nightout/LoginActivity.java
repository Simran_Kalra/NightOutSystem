package com.example.simran.nightout;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private EditText edtusername;
    private EditText edtpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void login(View view) {
        edtusername = (EditText) findViewById(R.id.edt_username);
        edtpassword = (EditText) findViewById(R.id.edt_password);
        String password = edtpassword.getText().toString();
        final String username = edtusername.getText().toString();

        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (e == null) {
                    Log.i("Log", "Successful");

                    /*ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
                    query.whereEqualTo("username", "admin");
                    query.setLimit(1);
                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {
                            if (e == null)
                                Log.i("OOobject:", "Retrieved Objects" + objects.size());
                            if (objects.size() > 1) {
                                for (ParseObject object : objects) {
                                    Log.i("user type:", object.getString("username"));
                                }
                            }
                        }
                    });*/

                    if(username.equals("admin")){
                        Intent intent=new Intent(LoginActivity.this,AdminHomeActivity.class);
                        startActivity(intent);
                    }else {
                        Intent intent=new Intent(LoginActivity.this,StudentActivity.class);
                        intent.putExtra("username",username);
                        startActivity(intent);
                    }

                }
                else {
                    Toast.makeText(LoginActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}

