package com.example.simran.nightout;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class NightOutList extends AppCompatActivity implements View.OnClickListener{

    private SimpleDateFormat dateFormatter;
    private EditText toDateEtxt;
    private DatePickerDialog toDatePickerDialog;
    String date;
    int month;
    int day;
    String dateString;
    int year;
    int monthInt;

    EditText edtdate;
    String[] name;
    String[] time;
    String[] room;
    String[] message;
    String[] phone;
    String address;
    int i;
    public void search(View view){
        edtdate=(EditText)findViewById(R.id.edt_date);
        String date=edtdate.getText().toString();
        ParseQuery<ParseObject>query=new ParseQuery<ParseObject>("Students");
        query.whereEqualTo("date",date);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                i=0;
                name=new String[objects.size()];
                time=new String[objects.size()];
                room=new String[objects.size()];
                message=new String[objects.size()];
                phone=new String[objects.size()];
                for(ParseObject object:objects){
                    Log.i(object.getString("name"),"1");
                    Log.i(object.getString("time"),"1");
                    address=object.getString("address");
                    phone[i]=object.getString("contact");
                    name[i]=object.getString("name");
                    time[i]=object.getString("time");
                    room[i]= String.valueOf(object.get("room"));
                    message[i]="Your child "+name[i]+" has left from hostel at "+time[i]+" for "+ address;
                    Log.i("object",name[i]);
                    i++;
                }
                ListView listView=(ListView) findViewById(R.id.list);
                myListAdapter myListAdapter=new myListAdapter(NightOutList.this,name,time,room);
                listView.setAdapter(myListAdapter);
            }

        });
    }
    public void message(View view){
        Log.i("i",String.valueOf(i));
        int k;
        for(k=i-1;k>=0;k--) {
            composeMmsMessage(message[k], phone[k]);
        }
    }
    public void composeMmsMessage(String message,String phoneNumber ) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("smsto:" + phoneNumber));  // This ensures only SMS apps respond
        intent.putExtra("sms_body", message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_night_out_list);

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        toDateEtxt = (EditText) findViewById(R.id.edt_date);
        toDateEtxt.setInputType(InputType.TYPE_NULL);

        setDateTimeField();
    }

    private void setDateTimeField() {
        toDateEtxt.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
//                monthInt=newDate.get(year);
//                month=monthInt+1;
                EditText dateString=(EditText)findViewById(R.id.edt_date);
                String date=dateString.getText().toString();
                Log.i("date",date);
                /*String[]dateParts = date.split("-");
                year = Integer.parseInt(dateParts[1]);
                month = Integer.parseInt(dateParts[0]);
                //day = Integer.parseInt(dateParts[0]);*/
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View v) {
        if(v == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }
}
