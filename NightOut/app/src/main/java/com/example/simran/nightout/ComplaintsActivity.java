package com.example.simran.nightout;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ComplaintsActivity extends AppCompatActivity {

    String username;
    String name;
    String room;
    String contact;

    EditText edt_name;
    EditText edt_room;
    EditText edt_contact;
    EditText ed_time;
    EditText ed_date;
    EditText edt_details;

    String selected;
    String[] components;
    String dateString;
    String timeString;

    public void submit(View view) {
        ParseObject object = new ParseObject("Complaints");
        object.put("username", username);
        object.put("name",name);
        object.put("room",edt_room.getText().toString());
        object.put("component",selected);
        object.put("contacts",edt_contact.getText().toString());
        object.put("time",timeString);
        object.put("date",dateString);
        object.put("details",edt_details.getText().toString());

        object.saveInBackground(new SaveCallback () {
            @Override
            public void done(ParseException ex) {
                if (ex == null) {
                    Toast.makeText(ComplaintsActivity.this, "Saved Successfully", Toast.LENGTH_LONG).show();
                    Log.i("Parse Result", "Successful!");
                } else {
                    Log.i("Parse Result", "Failed" + ex.toString());
                }
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);

        edt_details=(EditText)findViewById(R.id.edt_details);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        final String[] components = new String[]{"fan", "light", "cupboard", "bed", "window"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, components);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("selected",components[position]);
                selected=components[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Intent intent=getIntent();
        username=intent.getStringExtra("username");
        Log.i("username",username);

        edt_name=(EditText) findViewById(R.id.edt_name);
        edt_room=(EditText) findViewById(R.id.edt_room);
        edt_contact=(EditText) findViewById(R.id.edt_contact);
        ed_time=(EditText)findViewById(R.id.edt_time);
        ed_date=(EditText)findViewById(R.id.edt_date);

        ParseQuery<ParseObject> query=ParseQuery.getQuery("Students");
        query.whereEqualTo("username",username);
        query.setLimit(1);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                Log.i("retrieved",String.valueOf(objects.size()));
                for(ParseObject object:objects) {
                    //Log.i(object.getString("name")+String.valueOf(object.get("room"))+object.getString("contact"),"Retrieved");
                    name=object.getString("name");
                    //Log.i("name",name);
                    room=String.valueOf(object.get("room"));
                    contact=object.getString("contact");
                }
                edt_name.setText(name);
                edt_room.setText(room);
                edt_contact.setText(contact);
            }
        });


        SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        //Log.i(format_date.format(date),"Date");
        dateString=format_date.format(date);

        SimpleDateFormat format_time = new SimpleDateFormat("HH:mm:ss");
        //Log.i(format_time.format(date),"Time");
        timeString=format_time.format(date);


        ed_date.setText(dateString);
        ed_time.setText(timeString);

    }
}
