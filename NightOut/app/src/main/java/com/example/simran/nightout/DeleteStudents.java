package com.example.simran.nightout;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class DeleteStudents extends AppCompatActivity {

    ListView listView;
    String[] name;
    String[] contact;
    String[] room;
    int i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_students);
        //final ArrayAdapter arrayAdapter1=new ArrayAdapter(this,android.R.layout.simple_list_item_2,contact);
        Spinner spinner=(Spinner) findViewById(R.id.dropdown);
        final String[]items=new String[]{"1","2","3","4"};
        final ArrayAdapter<String>adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,items);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Log.i("position",items[position]);
                String year=items[position];
                search(year);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void search(String year){
        ParseQuery<ParseObject>query=new ParseQuery<ParseObject>("Students");
        query.whereEqualTo("year",year);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                Log.i("objects",String.valueOf(objects.size()));
                if(objects.size()>0){
                    name=new String[objects.size()];
                    contact=new String[objects.size()];
                    room=new String[objects.size()];
                    i=0;
                    for(ParseObject object:objects){
                        Log.i("object",object.getString("name"));
                        name[i]=object.getString("name");

                        Log.i("object",object.getString("contact"));
                        contact[i]=object.getString("contact");

                        Log.i("object",String.valueOf(object.getInt("room")));
                        room[i]=String.valueOf(object.getInt("room"));
                    }
                    final myListAdapter arrayAdapter=new myListAdapter(DeleteStudents.this,name,contact,room);
                    listView=findViewById(R.id.list);
                    arrayAdapter.notifyDataSetChanged();
                    listView.setAdapter(arrayAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            /*Intent intent=new Intent(ViewStudents.this,RegisterActivity.class);
                            intent.putExtra("name",name[position]);
                            intent.putExtra("room",room[position]);
                            intent.putExtra("contact",contact[position]);
                            Log.i("name:",name[position]);
                            startActivity(intent);*/
                            delete(name[position]);
                        }
                    });

                    //listView.setAdapter(arrayAdapter1);
                }
            }
        });
    }
    public void delete(String nm) {
        ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Students");
        query1.whereEqualTo("name", nm);
        query1.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                try {
                    object.delete();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                object.saveInBackground();
            }
        });
        listView.setAdapter(null);
    }
}
