package com.example.simran.nightout;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    String[] date1;
    String[] details;
    String[] component;
    String[] date;
    String[] time;
    String[] address;
    int i;
    String name;
    String contact;
    String room;
    EditText edt_date;
    EditText edt_address;
    EditText edt_time;
    EditText edt_name;
    EditText edt_room;
    EditText edt_contact;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent=getIntent();
        String username=intent.getStringExtra("username");
        Log.i("user",username);
        if(username!=null) {
            search(username);
        }
        edt_time=(EditText)findViewById(R.id.edt_time);
        edt_address=(EditText) findViewById(R.id.edt_address);
        edt_date=(EditText)findViewById(R.id.edt_date);
        edt_room=(EditText)findViewById(R.id.edt_room);
        edt_name=(EditText) findViewById(R.id.edt_name);
        edt_contact=(EditText)findViewById(R.id.edt_contact);
    }
    public void search(String username) {
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Students");
        query.whereEqualTo("username", username);
        query.findInBackground(new FindCallback<ParseObject>() {
            int i=0;
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                for(ParseObject object:objects) {
                    address=new String[objects.size()];
                    date=new String[objects.size()];
                    time=new String[objects.size()];
                    //Log.i(object.getString("name")+String.valueOf(object.get("room"))+object.getString("contact"),"Retrieved");
                    name=object.getString("name");
                    address[i]=object.getString("address");
                    Log.i("addr",object.getString("address"));
                    Log.i("addr",object.getString("date"));
                    date[i]=object.getString("date");
                    time[i]=object.getString("time");
                    room=String.valueOf(object.get("room"));
                    contact=object.getString("contact");
                    Log.i("addr",address[i]);
                    Log.i("addr",date[i]);
                    Log.i("addr",time[i]);
                    i++;
                }
                ListView listView=(ListView) findViewById(R.id.edt_list);
                myListAdapter myListAdapter=new myListAdapter(ProfileActivity.this,address,time,date);
                listView.setAdapter(myListAdapter);
                edt_name.setText(name);
                edt_room.setText(room);
                edt_contact.setText(contact);
            }
        });

        ParseQuery<ParseObject>query1=new ParseQuery<ParseObject>("Complaints");
        query1.whereEqualTo("username",username);
        query1.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                i=0;
                date1=new String[objects.size()];
                details=new String[objects.size()];
                component=new String[objects.size()];
                for(ParseObject object:objects){
                    details[i]=object.getString("details");
                    date1[i]=object.getString("date");
                    component[i]=object.getString("component");
                    i++;
                }
                ListView listView=(ListView) findViewById(R.id.edt_list1);
                myListAdapter myListAdapter=new myListAdapter(ProfileActivity.this,component,details,date1);
                listView.setAdapter(myListAdapter);
            }

        });
    }
}

