package com.example.simran.nightout;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.zip.Inflater;

public class AdminHomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    DrawerLayout drawer;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;

    public void init(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this,drawer,R.string.OpenNavigationDrawer,R.string.CloseNavigationDrawer);
        drawer.setDrawerListener(toggle);
        drawer.openDrawer(GravityCompat.START);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        init();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.nav_home){
            Intent intent=new Intent(this,AdminHomeActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.nav_registerStudents){
            Intent intent=new Intent(this,RegisterActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.nav_viewStudents){
            Intent intent=new Intent(this,ViewStudents.class);
            startActivity(intent);
        }
        else if(id==R.id.nav_deleteStudents){
            Intent intent=new Intent(this,DeleteStudents.class);
            startActivity(intent);
        }
        else if(id==R.id.nav_viewComplaints){
            Intent intent=new Intent(this,ViewComplaints.class);
            startActivity(intent);
        }
        else if(id==R.id.nav_viewNightout){
            Intent intent=new Intent(this,NightOutList.class);
            startActivity(intent);
        }
        else if(id==R.id.nav_logout){
            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen_drawer,menu);
        return true;
    }*/
}
