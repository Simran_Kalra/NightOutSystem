package com.example.simran.nightout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    String nm;
    String rm;
    String cont;

    EditText username;
    EditText room;
    EditText name;
    EditText password;
    EditText contact;
    EditText year;
    EditText parent;

    ParseObject object;

    public void register(View view) {

        object = new ParseObject("Students");

        ParseUser student = new ParseUser();

        student.setUsername(username.getText().toString());
        student.setPassword(password.getText().toString());
        student.put("name", name.getText().toString());
        student.put("parent", parent.getText().toString());
        student.put("room", Integer.parseInt(room.getText().toString()));
        student.put("contact", contact.getText().toString());
        student.put("year", year.getText().toString());
        student.put("type", "student");

        object.put("name", name.getText().toString());
        object.put("username", username.getText().toString());
        object.put("parent", parent.getText().toString());
        object.put("room", Integer.parseInt(room.getText().toString()));
        object.put("contact", contact.getText().toString());
        object.put("year", year.getText().toString());
        object.put("type", "student");
        object.put("password", password.getText().toString());

        object.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i("Student Table", "Created");
                } else {
                    Log.i("Student", "Not Created");
                }
            }
        });


        student.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(RegisterActivity.this, "Saved Successfully", Toast.LENGTH_LONG).show();

                    Log.i("Signed Up", "Successfully");
                }else
                    Log.i("Error", e.toString());
            }
        });
        student.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(RegisterActivity.this, "Saved Successfully", Toast.LENGTH_LONG).show();
                    Log.i("saved", "successfully");
                }
                else
                    Log.i("Error", e.toString());
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
       //ntent intent = getIntent();

        name = (EditText) findViewById(R.id.edt_name);
        username = (EditText) findViewById(R.id.stud_username);
        password = (EditText) findViewById(R.id.edt_password);
        parent = (EditText) findViewById(R.id.edt_parent);
        room = (EditText) findViewById(R.id.edt_room);
        contact = (EditText) findViewById(R.id.edt_contact);
        year = (EditText) findViewById(R.id.edt_year);


        /*m = intent.getStringExtra("name");
        rm = intent.getStringExtra("room");
        cont = intent.getStringExtra("contact");
        if (nm != null) {
            setTitle("Edit Student");
            try {
                fill_details(nm);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }*/
    }

   /*ublic void fill_details(String nm) throws ParseException {
        name.setText(nm);
        room.setText(rm);
        contact.setText(cont);

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Students");
        query.whereEqualTo("name", nm);
        query.setLimit(1);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                Log.i("objects", String.valueOf(objects.size()));
                if (objects.size() > 0) {
                    for (ParseObject object : objects) {
                        username.setText(object.getString("username"));
                        password.setText(object.getString("password"));
                        year.setText(object.getString("year"));
                        parent.setText(object.getString("parent"));
                    }
                }
            }
        });
        //update(nm);
        //delete(nm);
    }
    public  void update(String nm){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
        query.whereEqualTo("name",nm);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null && list.size()>0) {
                    ParseObject person = list.get(0);
                    person.put("name", "John");
                    person.saveInBackground();
                } else {
                    Log.d("Error: " , e.getMessage());
                }
            }
        });
    }

    public void delete(String nm) {
        ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Students");
        query1.whereEqualTo("name", nm);
        query1.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                try {
                    object.delete();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                object.saveInBackground();
            }
        });
    }*/
}



