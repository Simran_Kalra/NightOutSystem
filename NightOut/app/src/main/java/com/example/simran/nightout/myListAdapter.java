package com.example.simran.nightout;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by simran on 1/4/18.
 */

public class myListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] name;
    private final String[] time;
    private final String[] room;

    public myListAdapter(Activity context, String[] name, String[] time,String[] room) {
        super(context, R.layout.nightoutlist, name);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.name=name;
        this.time=time;
        this.room=room;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.nightoutlist, null,true);

        TextView nameText = (TextView) rowView.findViewById(R.id.name);
        TextView timeText = (TextView) rowView.findViewById(R.id.time);
        TextView roomText = (TextView) rowView.findViewById(R.id.txt_room);

        nameText.setText(name[position]);
        timeText.setText(time[position]);
        roomText.setText(room[position]);
        return rowView;
    };
}
