package com.example.simran.nightout;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class ViewComplaints extends AppCompatActivity {

    int i;
    String[] name;
    String[] details;
    String[] component;
    String room;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_complaints);
        //final ArrayAdapter arrayAdapter1=new ArrayAdapter(this,android.R.layout.simple_list_item_2,contact);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        final String[] items = new String[]{"106", "107", "108", "109"};
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Log.i("position",items[position]);
                //int pos= Integer.parseInt(items[position]);
                room = items[position];
                search(room);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void search(String room){
        ParseQuery<ParseObject>query=new ParseQuery<ParseObject>("Complaints");
        query.whereEqualTo("room",room);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                i=0;
                name=new String[objects.size()];
                details=new String[objects.size()];
                component=new String[objects.size()];
                for(ParseObject object:objects){
                    Log.i(object.getString("name"),"1");
                    name[i]=object.getString("name");
                    details[i]=object.getString("details");
                    Log.i(object.getString("component"),"1");
                    component[i]=object.getString("component");
                    i++;
                }
                ListView listView=(ListView) findViewById(R.id.list);
                myListAdapter myListAdapter=new myListAdapter(ViewComplaints.this,name,details,component);
                listView.setAdapter(myListAdapter);
            }

        });
    }
}

