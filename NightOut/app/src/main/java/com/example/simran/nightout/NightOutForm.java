package com.example.simran.nightout;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class NightOutForm extends AppCompatActivity {

    String name;
    String room;
    String contact;

    EditText edt_name;
    EditText edt_room;
    EditText edt_contact;
    EditText ed_time;
    EditText ed_date;
    EditText ed_address;

    String dateString;
    String timeString;
    String username;

    public void submit(View view) {
        final String address = ed_address.getText().toString();

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Students");
        query.whereEqualTo("username", username);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                for (ParseObject object : objects) {
                    object.put("address", address);
                    object.put("time", timeString);
                    object.put("date", dateString);
                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Toast.makeText(NightOutForm.this, "Saved Successfully", Toast.LENGTH_LONG).show();
                                Log.i("Data", "Saved");
                            }else
                                Log.i("Error", e.toString());
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_night_out_form);
        Intent intent=getIntent();
        username=intent.getStringExtra("username");
        Log.i("username",username);

        edt_name=(EditText) findViewById(R.id.edt_name);
        edt_room=(EditText) findViewById(R.id.edt_room);
        edt_contact=(EditText) findViewById(R.id.edt_contact);
        ed_time=(EditText)findViewById(R.id.edt_time);
        ed_date=(EditText)findViewById(R.id.edt_date);
        ed_address=(EditText)findViewById(R.id.edt_address);

        ParseQuery<ParseObject>query=ParseQuery.getQuery("Students");
        query.whereEqualTo("username",username);
        query.setLimit(1);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                Log.i("retrieved",String.valueOf(objects.size()));
                for(ParseObject object:objects) {
                    //Log.i(object.getString("name")+String.valueOf(object.get("room"))+object.getString("contact"),"Retrieved");
                    name=object.getString("name");
                    //Log.i("name",name);
                    room=String.valueOf(object.get("room"));
                    contact=object.getString("contact");
                }
                edt_name.setText(name);
                edt_room.setText(room);
                edt_contact.setText(contact);
            }
        });


        SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        //Log.i(format_date.format(date),"Date");
        dateString=format_date.format(date);

        SimpleDateFormat format_time = new SimpleDateFormat("HH:mm:ss");
        //Log.i(format_time.format(date),"Time");
        timeString=format_time.format(date);


        ed_date.setText(dateString);
        ed_time.setText(timeString);
    }
}
